-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-03-2017 a las 13:15:02
-- Versión del servidor: 5.7.16-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examen1703`
--
CREATE DATABASE IF NOT EXISTS `examen1703` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `examen1703`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authors`
--

DROP TABLE IF EXISTS `authors`;
CREATE TABLE `authors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `birth_year` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `authors`
--

INSERT INTO `authors` (`id`, `name`, `country`, `birth_year`, `created_at`, `updated_at`) VALUES
(1, 'Sra. Mar Marcos', 'Maldivas', 1954, '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(2, 'Alicia Esparza', 'Egipto', 1923, '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(3, 'Ruben Zambrano', 'Cuba', 1960, '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(4, 'Lic. Adam Camacho', 'Rusia', 1851, '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(5, 'Elena Arribas', 'Filipinas', 1961, '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(6, 'Olivia Rodríquez', 'Ghana', 1769, '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(7, 'Leo Laureano Tercero', 'Benín', 1974, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(8, 'Ing. Angela Almonte Hijo', 'Togo', 1767, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(9, 'Santiago Maldonado', 'Austria', 1953, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(10, 'Patricia Gamboa', 'Botsuana', 1939, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(11, 'Cesar Peres', 'Irlanda', 1821, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(12, 'Mireia Villanueva', 'Bahamas', 1931, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(13, 'Silvia Águilar Hijo', 'República Centroafricana', 1844, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(14, 'Dn. Marco Otero Segundo', 'Malaui', 1983, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(15, 'Valeria Serrato', 'Brasil', 1940, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(16, 'Nuria León', 'Dominica', 1850, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(17, 'Lic. Fatima Saldivar', 'Marruecos', 1834, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(18, 'Carlos Arriaga', 'Singapur', 1806, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(19, 'Paula Collazo', 'República Centroafricana', 1939, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(20, 'Alonso Muñoz', 'Rumanía', 1754, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(21, 'Elena Becerra Segundo', 'Kiribati', 1958, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(22, 'Laura Montemayor', 'República Dominicana', 1869, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(23, 'Srita. Silvia Ocasio', 'Catar', 1806, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(24, 'Rodrigo Terrazas Hijo', 'Jordania', 1806, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(25, 'Alicia Toro Hijo', 'Hungría', 1796, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(26, 'Carlos Leiva', 'México', 1915, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(27, 'Oscar Marín', 'Georgia', 1946, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(28, 'Bruno Ballesteros Tercero', 'Costa Rica', 1909, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(29, 'Iker Ávalos', 'Catar', 1757, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(30, 'Clara Rojas', 'Zimbabue', 1896, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(31, 'Pau Núñez Segundo', 'Libia', 1988, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(32, 'Ing. Luna Pardo Hijo', 'Bangladés', 1962, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(33, 'Silvia Chapa', 'Nauru', 1829, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(34, 'Carla Cerda', 'Costa de Marfil', 1957, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(35, 'Diego Ruvalcaba', 'Belice', 1822, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(36, 'Nadia Saldaña', 'Camerún', 1800, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(37, 'Eric Agosto', 'México', 1976, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(38, 'Guillem Echevarría', 'Moldavia', 1805, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(39, 'Sara Madrid Segundo', 'Mauritania', 1817, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(40, 'Dr. Adriana Haro Segundo', 'Angola', 1939, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(41, 'Cesar Serrato', 'Albania', 1754, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(42, 'Omar Rincón', 'Surinam', 1888, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(43, 'Arnau Muñiz', 'Papúa Nueva Guinea', 1879, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(44, 'Carla Vega', 'Brunéi Darusalam', 1849, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(45, 'Srita. Teresa Barraza', 'Brunéi Darusalam', 1881, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(46, 'Sandra Acuña Hijo', 'Mauritania', 1829, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(47, 'Aitana Arreola', 'España', 1949, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(48, 'Anna Maldonado', 'Chipre', 1965, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(49, 'Ing. Ariadna Preciado Tercero', 'Islandia', 1886, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(50, 'Eduardo Acosta', 'República Democrática del Congo', 1897, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(51, 'Lic. Joan Meraz', 'Portugal', 1970, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(52, 'Marti Serrano', 'Jamaica', 1853, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(53, 'Jose Alonzo', 'Ciudad del Vaticano', 1776, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(54, 'Eduardo Valles', 'República Centroafricana', 1786, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(55, 'Berta Carrión Hijo', 'Gambia', 1909, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(56, 'Andrea Jimínez', 'Ucrania', 1823, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(57, 'Ariadna Vega', 'Luxemburgo', 1952, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(58, 'Roberto Casanova', 'Tonga', 1865, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(59, 'Ing. Francisco Rangel', 'Irak', 1982, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(60, 'Sra. Victoria Ornelas Hijo', 'Mónaco', 1960, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(61, 'Lic. Rocio Benavídez Tercero', 'República Checa', 1771, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(62, 'Ander Valdivia Segundo', 'Mali', 1980, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(63, 'Irene Palomino', 'Birmania', 1931, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(64, 'Dr. Ignacio Correa Hijo', 'Guyana', 1850, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(65, 'Angel Fierro', 'Fiyi', 1946, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(66, 'Nil Puig', 'Kuwait', 1762, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(67, 'Dr. Anna Madrid', 'Dinamarca', 1900, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(68, 'Dr. Nadia Gallego', 'Nicaragua', 1931, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(69, 'Ainara Pantoja', 'Zimbabue', 1818, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(70, 'Erik Hernando', 'Panamá', 1968, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(71, 'Dr. Alejandra Leyva Tercero', 'Mongolia', 1880, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(72, 'Dn. Jan Paredes', 'Camboya', 1822, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(73, 'Francisco Villalpando', 'Bielorrusia', 1866, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(74, 'Vega Rueda', 'Haití', 1768, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(75, 'Dr. Leire Álvarez Hijo', 'Canadá', 1922, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(76, 'Helena de Jesús', 'Birmania', 1756, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(77, 'Victor Chávez', 'Birmania', 1848, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(78, 'Ruben Luna', 'Togo', 1880, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(79, 'Nayara Ybarra Segundo', 'Barbados', 1971, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(80, 'Jose Manuel Villegas', 'Uganda', 1985, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(81, 'Laia Rico', 'Mozambique', 1787, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(82, 'Joel Longoria', 'Chile', 1951, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(83, 'Jan Salinas', 'Eritrea', 1847, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(84, 'Blanca Guevara', 'Estados Unidos de América', 1814, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(85, 'Sra. Naia Luque Segundo', 'Liechtenstein', 1987, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(86, 'Naiara Moran', 'Chile', 1766, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(87, 'Berta Echevarría', 'Bahamas', 1887, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(88, 'Srita. Noelia Tejeda', 'Bosnia-Herzegovina', 1948, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(89, 'Sr. Gerard Guardado Tercero', 'Tonga', 1882, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(90, 'Guillermo Hernandes Segundo', 'Sudán', 1879, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(91, 'Alma Garay', 'Irán', 1917, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(92, 'Miriam Puga', 'Francia', 1903, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(93, 'Naia Adame', 'Canadá', 1868, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(94, 'Sra. Lara Casas', 'Rusia', 1857, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(95, 'David Cavazos', 'Níger', 1763, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(96, 'Sergio Molina', 'Samoa', 1823, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(97, 'Lic. Andrea Villalpando', 'Yemen', 1870, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(98, 'Srita. Alba Gimeno', 'Ucrania', 1914, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(99, 'Samuel Casares Hijo', 'Costa Rica', 1764, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(100, 'Aleix Rodrigo', 'Polonia', 1939, '2017-03-20 09:07:19', '2017-03-20 09:07:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `author_book`
--

DROP TABLE IF EXISTS `author_book`;
CREATE TABLE `author_book` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `book_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `author_book`
--

INSERT INTO `author_book` (`id`, `author_id`, `book_id`, `created_at`, `updated_at`) VALUES
(1, 43, 1, NULL, NULL),
(2, 38, 2, NULL, NULL),
(3, 73, 2, NULL, NULL),
(4, 97, 2, NULL, NULL),
(5, 41, 3, NULL, NULL),
(6, 53, 3, NULL, NULL),
(7, 76, 3, NULL, NULL),
(8, 78, 4, NULL, NULL),
(9, 97, 4, NULL, NULL),
(10, 45, 5, NULL, NULL),
(11, 58, 5, NULL, NULL),
(12, 74, 5, NULL, NULL),
(13, 16, 6, NULL, NULL),
(14, 87, 6, NULL, NULL),
(15, 97, 6, NULL, NULL),
(16, 72, 7, NULL, NULL),
(17, 91, 7, NULL, NULL),
(18, 33, 8, NULL, NULL),
(19, 53, 8, NULL, NULL),
(20, 76, 8, NULL, NULL),
(21, 54, 9, NULL, NULL),
(22, 53, 10, NULL, NULL),
(23, 75, 10, NULL, NULL),
(24, 22, 11, NULL, NULL),
(25, 64, 11, NULL, NULL),
(26, 72, 12, NULL, NULL),
(27, 20, 13, NULL, NULL),
(28, 36, 13, NULL, NULL),
(29, 95, 14, NULL, NULL),
(30, 16, 15, NULL, NULL),
(31, 82, 15, NULL, NULL),
(32, 53, 16, NULL, NULL),
(33, 87, 17, NULL, NULL),
(34, 93, 17, NULL, NULL),
(35, 72, 18, NULL, NULL),
(36, 74, 18, NULL, NULL),
(37, 84, 18, NULL, NULL),
(38, 25, 19, NULL, NULL),
(39, 38, 19, NULL, NULL),
(40, 58, 19, NULL, NULL),
(41, 72, 20, NULL, NULL),
(42, 95, 20, NULL, NULL),
(43, 49, 21, NULL, NULL),
(44, 59, 21, NULL, NULL),
(45, 72, 21, NULL, NULL),
(46, 97, 22, NULL, NULL),
(47, 86, 23, NULL, NULL),
(48, 78, 24, NULL, NULL),
(49, 97, 24, NULL, NULL),
(50, 42, 25, NULL, NULL),
(51, 25, 26, NULL, NULL),
(52, 76, 26, NULL, NULL),
(53, 28, 27, NULL, NULL),
(54, 68, 27, NULL, NULL),
(55, 13, 28, NULL, NULL),
(56, 16, 28, NULL, NULL),
(57, 86, 29, NULL, NULL),
(58, 99, 29, NULL, NULL),
(59, 83, 30, NULL, NULL),
(60, 87, 31, NULL, NULL),
(61, 41, 32, NULL, NULL),
(62, 45, 32, NULL, NULL),
(63, 29, 33, NULL, NULL),
(64, 53, 33, NULL, NULL),
(65, 81, 33, NULL, NULL),
(66, 58, 34, NULL, NULL),
(67, 86, 34, NULL, NULL),
(68, 54, 35, NULL, NULL),
(69, 8, 36, NULL, NULL),
(70, 53, 36, NULL, NULL),
(71, 76, 36, NULL, NULL),
(72, 90, 37, NULL, NULL),
(73, 65, 38, NULL, NULL),
(74, 53, 39, NULL, NULL),
(75, 61, 39, NULL, NULL),
(76, 36, 40, NULL, NULL),
(77, 95, 40, NULL, NULL),
(78, 76, 41, NULL, NULL),
(79, 96, 41, NULL, NULL),
(80, 93, 42, NULL, NULL),
(81, 97, 42, NULL, NULL),
(82, 45, 43, NULL, NULL),
(83, 90, 43, NULL, NULL),
(84, 22, 44, NULL, NULL),
(85, 44, 44, NULL, NULL),
(86, 94, 44, NULL, NULL),
(87, 35, 45, NULL, NULL),
(88, 89, 45, NULL, NULL),
(89, 56, 46, NULL, NULL),
(90, 76, 46, NULL, NULL),
(91, 30, 47, NULL, NULL),
(92, 35, 47, NULL, NULL),
(93, 75, 47, NULL, NULL),
(94, 29, 48, NULL, NULL),
(95, 66, 49, NULL, NULL),
(96, 81, 49, NULL, NULL),
(97, 53, 50, NULL, NULL),
(98, 95, 50, NULL, NULL),
(99, 66, 51, NULL, NULL),
(100, 84, 51, NULL, NULL),
(101, 86, 51, NULL, NULL),
(102, 28, 52, NULL, NULL),
(103, 43, 52, NULL, NULL),
(104, 91, 52, NULL, NULL),
(105, 17, 53, NULL, NULL),
(106, 35, 53, NULL, NULL),
(107, 86, 53, NULL, NULL),
(108, 56, 54, NULL, NULL),
(109, 99, 54, NULL, NULL),
(110, 89, 55, NULL, NULL),
(111, 97, 55, NULL, NULL),
(112, 35, 56, NULL, NULL),
(113, 60, 56, NULL, NULL),
(114, 80, 56, NULL, NULL),
(115, 24, 57, NULL, NULL),
(116, 33, 57, NULL, NULL),
(117, 86, 57, NULL, NULL),
(118, 72, 58, NULL, NULL),
(119, 77, 58, NULL, NULL),
(120, 42, 59, NULL, NULL),
(121, 50, 59, NULL, NULL),
(122, 29, 60, NULL, NULL),
(123, 44, 60, NULL, NULL),
(124, 53, 61, NULL, NULL),
(125, 56, 61, NULL, NULL),
(126, 98, 61, NULL, NULL),
(127, 61, 62, NULL, NULL),
(128, 100, 62, NULL, NULL),
(129, 39, 63, NULL, NULL),
(130, 61, 63, NULL, NULL),
(131, 81, 63, NULL, NULL),
(132, 44, 64, NULL, NULL),
(133, 45, 65, NULL, NULL),
(134, 55, 65, NULL, NULL),
(135, 76, 66, NULL, NULL),
(136, 77, 67, NULL, NULL),
(137, 81, 67, NULL, NULL),
(138, 36, 68, NULL, NULL),
(139, 41, 68, NULL, NULL),
(140, 99, 68, NULL, NULL),
(141, 70, 69, NULL, NULL),
(142, 79, 69, NULL, NULL),
(143, 91, 70, NULL, NULL),
(144, 45, 71, NULL, NULL),
(145, 47, 71, NULL, NULL),
(146, 95, 71, NULL, NULL),
(147, 35, 72, NULL, NULL),
(148, 63, 72, NULL, NULL),
(149, 69, 73, NULL, NULL),
(150, 97, 73, NULL, NULL),
(151, 76, 74, NULL, NULL),
(152, 18, 75, NULL, NULL),
(153, 29, 75, NULL, NULL),
(154, 24, 76, NULL, NULL),
(155, 81, 76, NULL, NULL),
(156, 31, 77, NULL, NULL),
(157, 81, 77, NULL, NULL),
(158, 64, 78, NULL, NULL),
(159, 47, 79, NULL, NULL),
(160, 100, 79, NULL, NULL),
(161, 33, 80, NULL, NULL),
(162, 56, 80, NULL, NULL),
(163, 83, 80, NULL, NULL),
(164, 69, 81, NULL, NULL),
(165, 78, 81, NULL, NULL),
(166, 96, 82, NULL, NULL),
(167, 44, 83, NULL, NULL),
(168, 93, 83, NULL, NULL),
(169, 96, 83, NULL, NULL),
(170, 69, 84, NULL, NULL),
(171, 96, 84, NULL, NULL),
(172, 41, 85, NULL, NULL),
(173, 64, 85, NULL, NULL),
(174, 84, 86, NULL, NULL),
(175, 65, 87, NULL, NULL),
(176, 72, 87, NULL, NULL),
(177, 83, 87, NULL, NULL),
(178, 35, 88, NULL, NULL),
(179, 92, 88, NULL, NULL),
(180, 77, 89, NULL, NULL),
(181, 94, 89, NULL, NULL),
(182, 87, 90, NULL, NULL),
(183, 63, 91, NULL, NULL),
(184, 56, 92, NULL, NULL),
(185, 95, 92, NULL, NULL),
(186, 66, 93, NULL, NULL),
(187, 66, 94, NULL, NULL),
(188, 74, 94, NULL, NULL),
(189, 76, 94, NULL, NULL),
(190, 76, 95, NULL, NULL),
(191, 18, 96, NULL, NULL),
(192, 38, 96, NULL, NULL),
(193, 81, 96, NULL, NULL),
(194, 99, 97, NULL, NULL),
(195, 50, 98, NULL, NULL),
(196, 90, 98, NULL, NULL),
(197, 99, 98, NULL, NULL),
(198, 25, 99, NULL, NULL),
(199, 93, 99, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pages` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `gender_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `books`
--

INSERT INTO `books` (`id`, `title`, `pages`, `year`, `gender_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Quisquam qui deleniti deserunt pariatur sed enim e', 314, 1930, 1, 1, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(2, 'Facilis in ea magni necessitatibus qui ut reiciend', 401, 1913, 4, 1, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(3, 'Exercitationem et aliquid exercitationem et error ', 353, 1801, 1, 2, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(4, 'Ab nihil eos enim aut dolores.', 443, 1916, 2, 3, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(5, 'Harum et commodi vel ad dolorem minus sunt.', 75, 1938, 2, 1, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(6, 'Odio eos voluptatem hic dolorem iusto.', 455, 1889, 3, 3, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(7, 'Ipsam nihil enim adipisci inventore voluptas.', 334, 1923, 1, 1, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(8, 'Dicta occaecati eum qui explicabo dicta ut qui.', 145, 1866, 4, 4, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(9, 'Et expedita nostrum tenetur rerum.', 55, 1924, 3, 2, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(10, 'Omnis officia asperiores et.', 59, 1933, 1, 1, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(11, 'Qui sed consequatur dolorem accusantium.', 240, 1878, 4, 1, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(12, 'Vero impedit ea alias aut suscipit.', 276, 1837, 3, 1, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(13, 'Repellendus sit esse molestiae occaecati et laboru', 197, 1814, 3, 3, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(14, 'Et voluptas impedit labore repellat qui omnis.', 106, 1828, 2, 4, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(15, 'Repudiandae qui sed ratione quod.', 286, 1952, 1, 3, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(16, 'Sed pariatur ipsum quis eveniet vel.', 132, 1986, 1, 2, '2017-03-20 09:07:19', '2017-03-20 09:07:19'),
(17, 'Omnis deleniti dolores labore et nisi.', 126, 1890, 2, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(18, 'Provident occaecati fugiat provident itaque enim i', 234, 1942, 4, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(19, 'Excepturi fugit impedit quod tenetur molestias vol', 61, 1870, 4, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(20, 'Dolorem corrupti fugit odit eligendi in.', 431, 1867, 4, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(21, 'Corrupti voluptatem non nam et aut voluptatem.', 345, 2013, 3, 2, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(22, 'Blanditiis illum harum placeat aspernatur qui simi', 282, 1907, 2, 2, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(23, 'Consectetur culpa voluptas laudantium et.', 410, 1895, 2, 3, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(24, 'Quibusdam consequatur quod voluptatem et odit ipsa', 305, 2013, 1, 3, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(25, 'Nostrum dolore repellendus in minima.', 132, 1989, 4, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(26, 'Consequatur dolorem totam magnam distinctio verita', 201, 1922, 4, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(27, 'Commodi molestias voluptatum et.', 465, 1987, 1, 3, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(28, 'In mollitia alias non suscipit occaecati.', 170, 1852, 4, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(29, 'Aut commodi voluptas eos architecto enim.', 77, 1900, 1, 2, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(30, 'Voluptatum quia illo facere et voluptas debitis.', 460, 1939, 1, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(31, 'Ut magnam voluptatem voluptatem.', 338, 1963, 2, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(32, 'Cupiditate hic et dolorem laborum in consequatur.', 226, 1933, 2, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(33, 'Cum eaque eos provident numquam eligendi sit volup', 462, 1813, 4, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(34, 'Excepturi asperiores pariatur sit in.', 326, 1893, 4, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(35, 'Veritatis quos id aperiam perspiciatis sapiente ea', 352, 1820, 3, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(36, 'Nostrum vero est quia et atque et.', 475, 1947, 1, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(37, 'Unde facilis iure doloribus aut.', 136, 1895, 2, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(38, 'Cum reprehenderit omnis exercitationem dolorem qua', 254, 2013, 4, 2, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(39, 'Beatae perferendis esse voluptas voluptas itaque a', 394, 1843, 2, 3, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(40, 'Qui ut adipisci minus hic nihil neque veritatis er', 291, 1801, 1, 3, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(41, 'Dolore aliquam dolores fugit enim enim repellat un', 174, 1904, 4, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(42, 'Aliquid explicabo quod hic blanditiis veritatis qu', 289, 1993, 3, 3, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(43, 'Maxime dolore repellendus unde et id itaque.', 136, 1888, 3, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(44, 'Sed alias corporis et voluptatum quisquam.', 191, 1895, 4, 2, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(45, 'Illum quibusdam est ex ea.', 89, 1926, 1, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(46, 'Sunt cupiditate voluptatem rerum deleniti dolorem.', 111, 1853, 1, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(47, 'Facere dolorem dolorum ea est quibusdam.', 354, 1972, 2, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(48, 'Officia commodi fuga adipisci necessitatibus venia', 157, 1819, 3, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(49, 'Temporibus porro vel est recusandae vel recusandae', 125, 1805, 4, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(50, 'Inventore accusantium odio porro dolorem et et eum', 358, 2005, 2, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(51, 'Laborum consequuntur adipisci similique sapiente i', 267, 1815, 1, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(52, 'Sit qui ipsa quidem inventore voluptas ut aut.', 173, 1990, 1, 2, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(53, 'Est doloremque quis itaque.', 79, 1877, 4, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(54, 'Repellat et et et est qui.', 265, 1832, 4, 1, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(55, 'Sit occaecati eveniet accusamus tempore quae unde.', 417, 1914, 1, 2, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(56, 'Voluptatibus error corporis praesentium sunt volup', 495, 2013, 4, 2, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(57, 'Quae quis dolor eius sit impedit.', 276, 1834, 3, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(58, 'Ea omnis exercitationem nesciunt commodi esse quia', 269, 1849, 2, 4, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(59, 'Aut in sunt sunt enim voluptatem qui ut.', 146, 1906, 4, 2, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(60, 'Possimus deleniti et perspiciatis nobis in sequi m', 246, 1943, 2, 3, '2017-03-20 09:07:20', '2017-03-20 09:07:20'),
(61, 'Et totam fugit quasi consequatur ea earum iusto.', 390, 1926, 2, 2, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(62, 'Tempora modi est ex voluptas ut.', 430, 1974, 2, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(63, 'Aut quidem eius tempora doloribus aliquam repellen', 118, 1826, 4, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(64, 'Et eaque repudiandae nemo qui.', 423, 1935, 2, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(65, 'Et consequatur repudiandae aspernatur voluptatum i', 254, 1997, 4, 2, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(66, 'Rem voluptatibus vel maiores et omnis rem sit.', 119, 1964, 4, 3, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(67, 'Sapiente accusantium veniam voluptas modi distinct', 225, 1894, 1, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(68, 'At labore mollitia recusandae in quam.', 265, 1834, 4, 2, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(69, 'Rerum facilis enim eaque maiores cum vero non.', 431, 2004, 3, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(70, 'Facere ipsum facilis minus repellat et error archi', 339, 1962, 2, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(71, 'Dolores fugiat ut laudantium quisquam ipsa nobis q', 95, 1951, 1, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(72, 'Vero autem ea et sint est.', 58, 1987, 4, 3, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(73, 'At recusandae exercitationem magni assumenda.', 59, 1884, 1, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(74, 'Consequatur voluptatem laudantium aut sed unde opt', 338, 2010, 2, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(75, 'Omnis fugiat enim maiores aut a.', 435, 1807, 2, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(76, 'Alias laudantium perspiciatis nulla nesciunt dolor', 167, 1855, 3, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(77, 'Quod voluptates in ea est ut iusto veritatis.', 284, 2013, 3, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(78, 'Quo veniam enim numquam ut aliquam atque laborum.', 151, 1875, 3, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(79, 'Earum magni quae et enim sed consequuntur.', 483, 1963, 3, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(80, 'Rerum quo magnam amet dolorem quae corrupti.', 287, 1866, 2, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(81, 'Quis nihil ea aut sed praesentium.', 107, 1921, 1, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(82, 'Occaecati rerum rerum harum omnis excepturi consec', 196, 1849, 4, 3, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(83, 'A labore porro suscipit.', 135, 1943, 1, 3, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(84, 'Tempora omnis at quasi debitis.', 412, 1825, 4, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(85, 'Cum quaerat nulla qui rerum assumenda.', 226, 1971, 4, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(86, 'Odio ipsam iste dolore perspiciatis voluptatem.', 467, 1932, 4, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(87, 'Et pariatur impedit quaerat.', 231, 1980, 3, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(88, 'Deserunt beatae explicabo repudiandae possimus.', 66, 1911, 4, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(89, 'Consequatur aut voluptatum aut tempora.', 120, 1964, 3, 3, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(90, 'Quam dicta repellendus id voluptate atque veniam.', 154, 1956, 4, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(91, 'Corrupti eum et magnam velit.', 105, 2010, 3, 3, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(92, 'Adipisci quasi omnis eius qui qui laborum.', 247, 1839, 1, 3, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(93, 'In nihil voluptates omnis iusto sit aut.', 224, 1931, 2, 2, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(94, 'Quis vel dolor id.', 324, 1855, 4, 2, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(95, 'Itaque dolorum dolor aspernatur amet sed magni non', 396, 1810, 3, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(96, 'Porro totam itaque voluptatem a consequuntur persp', 165, 1829, 2, 4, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(97, 'Veritatis possimus neque commodi similique aut ex ', 58, 1988, 3, 2, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(98, 'Rem ut aut voluptatem illum.', 310, 1905, 1, 2, '2017-03-20 09:07:21', '2017-03-20 09:07:21'),
(99, 'Distinctio vel culpa quia laudantium molestiae.', 380, 1913, 3, 1, '2017-03-20 09:07:21', '2017-03-20 09:07:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genders`
--

DROP TABLE IF EXISTS `genders`;
CREATE TABLE `genders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `genders`
--

INSERT INTO `genders` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'comedia', '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(2, 'drama', '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(3, 'tragedia', '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(4, 'infantil', '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(5, 'juvenil', '2017-03-20 09:07:18', '2017-03-20 09:07:18'),
(6, 'poesía', '2017-03-20 09:07:18', '2017-03-20 09:07:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(491, '2014_10_12_000000_create_users_table', 1),
(492, '2014_10_12_100000_create_password_resets_table', 1),
(493, '2017_03_20_063141_create_genders_table', 1),
(494, '2017_03_20_063149_create_books_table', 1),
(495, '2017_03_20_063155_create_authors_table', 1),
(496, '2017_03_20_063228_create_author_book_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Pepe', 'pepe@gmail.com', '$2y$10$GymCtMTEDvNcJ8FGb6StLeLktDVf0ovZvBC0VBZ69p3MJxS02oj0q', NULL, NULL, NULL),
(2, 'Juan', 'juan@gmail.com', '$2y$10$4.lWccgudAqBny3Fc8yG1eTd3okCePIGGBdud/207k9mPiYWNvbvi', NULL, NULL, NULL),
(3, 'Ana', 'ana@gmail.com', '$2y$10$eF246hL9L8jkaEkzuDdRruJEEc43JoKpTJZdKB9E7B3aB6XRDTAdS', NULL, NULL, NULL),
(4, 'Yolanda', 'yolanda@gmail.com', '$2y$10$td1/vW4bxfd5Xa4LDFZALe9ICW7Xw9jmB0YVSW7Bsn/Esg4dDlVlW', NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `author_book`
--
ALTER TABLE `author_book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_book_author_id_foreign` (`author_id`),
  ADD KEY `author_book_book_id_foreign` (`book_id`);

--
-- Indices de la tabla `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `books_user_id_foreign` (`user_id`),
  ADD KEY `books_gender_id_foreign` (`gender_id`);

--
-- Indices de la tabla `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `genders_name_unique` (`name`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT de la tabla `author_book`
--
ALTER TABLE `author_book`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;
--
-- AUTO_INCREMENT de la tabla `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT de la tabla `genders`
--
ALTER TABLE `genders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=497;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `author_book`
--
ALTER TABLE `author_book`
  ADD CONSTRAINT `author_book_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `author_book_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_gender_id_foreign` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `books_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
